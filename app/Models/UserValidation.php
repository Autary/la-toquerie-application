<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserValidation extends Model
{
    use HasFactory;
    protected $primaryKey = 'validation_id';
    protected $table='profile_validation';
    public $timestamps = false;

    protected $fillable = [
        'validation_token',
        'validation_profile'
    ];
}
