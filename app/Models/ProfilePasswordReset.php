<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfilePasswordReset extends Model
{
    use HasFactory;
    protected $primaryKey = 'ppr_id';
    protected $table='profile_password_reset';
    public $timestamps = false;

    protected $fillable = [
        "ppr_token",
        "ppr_pwd",
        "ppr_profile"
    ];
}
