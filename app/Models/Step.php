<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Step extends Model
{
    use HasFactory;
    protected $primaryKey = 'step_id';
    protected $table='step';
    public $timestamps = false;

    protected $fillable = [
        "step_position",
        "step_name",
        "step_description",
        "step_realisationtime",
        "step_sleeptime",
        "step_fridgetime",
        "step_freezetime",
        "step_cooktime",
    ];

    public function composedBy()
    {
        return $this->hasMany(__NAMESPACE__.'\ComposedBy', 'composedby_stepid');
    }
}
