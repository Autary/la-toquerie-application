<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    private String $token;
    private $user;
    private $tokenDateLimit;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($pwdReset, $user)
    {
        $this->token = env('APP_ANGULAR_URL').'/password-validation?token='.$pwdReset->ppr_token;
        $this->tokenDateLimit = new \Carbon\Carbon($pwdReset->deleted_at);
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Procédure de changement de mot de passe')
            ->markdown('emails.registration.resetPassword')
            ->with('token', $this->token)
            ->with('tokenDateLimit', $this->tokenDateLimit)
            ->with('name', $this->user->profile_name);
    }
}
