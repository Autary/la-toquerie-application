<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notation extends Model
{
    use HasFactory;
    protected $primaryKey = 'notation_id';
    protected $table='notation';
    public $timestamps = false;
}
