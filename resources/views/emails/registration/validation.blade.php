@component('mail::message')
# Félicitations {{ $name }},

<div>
    <p>Vous venez de créer un compte sur La Toquerie, vous faites désormais partie des Toqués !</p>
    <p>Pour <strong>finaliser votre inscription</strong>, veuillez valider votre compte en <strong>cliquant sur le bouton</strong> ci-dessous. Vous pourrez ensuite vous connecter à votre compte et partager vos recettes !</p>
</div> 

@component('mail::button', ['url' => $token])
Valider mon compte
@endcomponent

Toquement votre,<br>
{{ config('app.name') }}
@endcomponent
