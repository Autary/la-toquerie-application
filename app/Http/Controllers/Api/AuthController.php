<?php
   
namespace App\Http\Controllers\Api;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use App\Models\ProfilePasswordReset;
use App\Models\UserValidation;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class AuthController extends BaseController
{
    public function signin(Request $request)
    {
        if(Auth::attempt(['profile_email' => $request->profile_email, 'password' => $request->profile_pwd])){ 
            $authUser = Auth::user(); 
            if($authUser->verified_at === null)
                return $this->sendError('Unverified', ['error' => 'email not verified'], 403);
                
            $success['token'] =  $authUser->createToken('MyAuthApp')->plainTextToken; 
            $success['profile_email'] =  $authUser->profile_email;
   
            return $this->sendResponse($success, 'User signed in');
        } 
        else{ 
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        } 
    }

    public function signup(Request $request)
    {
        try{
            \DB::beginTransaction();

            $validator = Validator::make($request->all(), [
                'profile_pseudo' => 'required|min:3|max:40',
                'profile_name' => 'required|max:150',
                'profile_email' => 'required|email|max:350',
                'profile_pwd' => 'required|min:8',
            ]);
    
            if($validator->fails()){
                return $this->sendError('Error validation', ['validators' => $validator->errors()]);       
            }
    
            $allowed = array('profile_pseudo', 'profile_name', 'profile_email', 'profile_pwd');
            $input = array_intersect_key($request->all(), array_flip($allowed));

            $input['profile_pwd'] = bcrypt($input['profile_pwd']);

            $user = User::create($input);

            $validationToken = Str::orderedUuid();

            UserValidation::create([
                'validation_profile' => $user->profile_id, 
                'validation_token' => $validationToken
            ]);

            $this->sendMail($user, $validationToken);
            
            \DB::commit();
            return $this->sendResponse(true, 'User created successfully.');
        } catch (\Illuminate\Database\QueryException $e){
            \DB::rollback();
            $errorMsg = $e->errorInfo[2];
            if(strpos($errorMsg, "uc_profile_pseudo") !== false){
                return $this->sendError('Registration error', [
                    "customCode" => "pseudo",
                    "customError" => "An account with this nickname already exists"
                ], 500); 
            } else if(strpos($errorMsg, "uc_profile_email") !== false){
                return $this->sendError('Registration error', [
                    "customCode" => "email",
                    "customError" => "An account with this email already exists"
                ], 500); 
            } else {
                return $this->sendError('Registration error, query exception', $e, 500);
            }                  
        } catch (\Exception $e){
            \DB::rollback();
            return $this->sendError('Registration error, server exception', $e->getMessage(), 500);
        }
        
    }
    public function sendMail($user, $validationToken){
        Mail::to($user->profile_email)->send(new \App\Mail\AccountValidation($validationToken, $user));
    }

    public function resendValidationMail(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'profile_email' => 'required|email',
            ]);
    
            if($validator->fails()){
                return $this->sendError('Error validation', $validator->errors());       
            }

            $user = User::where('profile_email', $request->profile_email)->first();
            if(!$user)
                throw new \Exception('No email founded');

            $userValidation = UserValidation::where('validation_profile', $user->profile_id)->first();
            if(!$userValidation)
                throw new \Exception('No token founded');            

            $this->sendMail($user, $userValidation->validation_token);
        } catch (\Exception $e){
            return $this->sendError('Send revalidation error', $e->getMessage(), 500);
        }
    }

    public function validateProfile(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'token' => 'required'
            ]);
    
            if($validator->fails()){
                return $this->sendError('Error validation', $validator->errors());       
            }

            $userValidation = UserValidation::where('validation_token', $request->token)->first();
            
            if(!$userValidation)
                return $this->sendError('Validation error', 'token not found', 403);

            \DB::beginTransaction();

            $res = User::where('profile_id', $userValidation->validation_profile)
                ->update(['verified_at' => \Carbon\Carbon::now()]);
            
            if(!$res)
                throw new \Exception('Failed to update user');

            $userValidation->delete();
            \DB::commit();
            return $this->sendResponse(true, 'User validated successfully.');            
        } catch (\Illuminate\Database\QueryException $e){
            \DB::rollback();
            $errorMsg = $e->errorInfo[2];
            return $this->sendError('Validation error, query exception', $e, 500);                
        } catch (\Exception $e){
            \DB::rollback();
            return $this->sendError('Validation error, server exception', $e->getMessage(), 500);
        }
    }

    public function createResetPassword(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'profile_email' => 'required|email',
                'profile_pwd' => 'required|min:8'
            ]);

            if($validator->fails()){
                return $this->sendError('Error validation', $validator->errors());       
            }
            
            \DB::beginTransaction();

            $user = User::where('profile_email', $request->profile_email)->first();

            //we don't precise if email exists or not (security)
            if($user){
                $otherResetRequest = ProfilePasswordReset::where([
                    ['ppr_profile', $user->profile_id],
                    ['ppr_end', '>=', \Carbon\Carbon::now()],
                    ['ppr_pwd', '<>', 'USED']
                ])->orderBy('ppr_end', 'desc')->first();
                if($otherResetRequest)
                    return $this->sendError('A request already exists', $otherResetRequest->ppr_end, 403);       

                $pwdReset = ProfilePasswordReset::create([
                    'ppr_token' => Str::orderedUuid(),
                    'ppr_pwd' => bcrypt($request->profile_pwd),
                    'ppr_profile' => $user->profile_id
                ]);
                
                Mail::to($user->profile_email)->send(new \App\Mail\ResetPassword($pwdReset, $user));
            }
            
            \DB::commit();
            return $this->sendResponse(null, "Reset ask has succceded");
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->sendError('Server exception', $e->getMessage(), 500);
        }
    }

    public function validateResetPassword(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'token' => 'required',
            ]);

            if($validator->fails()){
                return $this->sendError('Error validation', $validator->errors());       
            }
            
            \DB::beginTransaction();

            $pwdReset = ProfilePasswordReset::where([
                ['ppr_token', $request->token],
                ['ppr_end', '>', \Carbon\Carbon::now()],
                ['ppr_pwd', '<>', 'USED']
            ])->orderBy('ppr_end', 'desc')->first();

            if(!$pwdReset)
                return $this->sendError('Server exception', 'No password request', 404);

            $user = User::find($pwdReset->ppr_profile);
            if(!$user)
                return $this->sendError('No user founded for this password request', $otherResetRequest->ppr_id, 500);       

            $user->profile_pwd = $pwdReset->ppr_pwd;
            $pwdReset->ppr_pwd = 'USED';

            $user->save();
            $pwdReset->save();
            
            \DB::commit();
            return $this->sendResponse(null, "Reset ask has succceded");
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->sendError('Server exception', $e->getMessage(), 500);
        }
    }

    public function check(Request $request){
        return $this->sendResponse($request->user(), 'Token is valid');
    }

    public function revoke(Request $request){
        $request->user()->tokens()->delete();
    }
   
}