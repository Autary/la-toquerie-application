<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AccountValidation extends Mailable
{
    use Queueable, SerializesModels;

    private String $token;
    private $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(String $token, $user)
    {
        $this->token = env('APP_ANGULAR_URL').'/validation?token='.$token;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Bienvenue dans la Toquerie !')
            ->markdown('emails.registration.validation')
            ->with('token', $this->token)
            ->with('name', $this->user->profile_name);
    }
}
