<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComposedBy extends Model
{
    use HasFactory;
    protected $primaryKey = 'composedby_id';
    protected $table='composed_by';
    public $timestamps = false;

    protected $fillable = [
        "composedby_ingredientname",
        "quantity",
        "mesure",
    ];
}
