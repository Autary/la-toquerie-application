<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Target;
use App\Models\Recipe;

class TargetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Target::all();
    }
    public function list()
    {
        $res = [];
        $targets = Target::whereHas('recipes', function($q){$q->publicShowed();})->get();
        $targets->each(function($target) use (&$res){

            $recipes = Recipe::with('badge', 'regime')->whereHas('targets', function($query) use ($target){
                    $query->where('target.target_id', $target->target_id);
                })
            ->getAverageNotations()
            ->orderByAverageNotations()
            ->take(4)->get();
            
            $recipes->map(function($recipe){
                return RecipeController::format($recipe);
            });

            $res[]=[
                'target' => $target,
                'recipes' => $recipes,
            ];
        });

        return $res;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
