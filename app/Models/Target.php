<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
    use HasFactory;
    protected $primaryKey = 'target_id';
    protected $table='target';
    public $timestamps = false;

    public function recipes()
    {
        return $this->belongsToMany(__NAMESPACE__.'\Recipe', 'recipe_target','target_id','recipe_id');
    }
}
