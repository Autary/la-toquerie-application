<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    use HasFactory;
    protected $primaryKey = 'picture_id';
    protected $table='recipe_picture';
    public $timestamps = false;
}
