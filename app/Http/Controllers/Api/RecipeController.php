<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Recipe;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class RecipeController extends BaseController
{

    public const RECIPE_IMAGE_EXT = 'png';

    /**
     * Display a listing of the resource, used in home/search page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filters = json_decode($_REQUEST['filters'] ?? '', 1);
        $q = Recipe::with('badge', 'country', 'regime', 'steps', 'notations');
        if(!!$filters){
            if(isset($filters['targets']))
            {
                $q->whereHas('targets', function($query) use ($filters){
                    $query->whereIn('target.target_id', $filters['targets']);
                });
            }
            if(isset($filters['take']))
            {
                $q->take($filters['take']);
            }
        }
        $q->publicShowed();

        $q->getAverageNotations();

        $q->orderByAverageNotations();
        $recipes = $q->get();
        
        $recipes->map(function($recipe){
            return RecipeController::format($recipe);
        });
        return $recipes;
    }
    public static function format(Recipe $recipe)
    {
        // format recipe times
        $ttRealisation = 0;
        $ttAtRest = 0;
        $ttFreeze = 0;
        $ttBake = 0;
        $ttFridge = 0;

        $recipe->steps->each(function($step) use (&$ttRealisation, &$ttAtRest, &$ttBake, &$ttFreeze, &$ttFridge){
            $step->step_realisationtime && $ttRealisation += \Carbon\Carbon::parse($step->step_realisationtime)->timestamp;
            $step->step_sleeptime && $ttAtRest += \Carbon\Carbon::parse($step->step_sleeptime)->timestamp;
            $step->step_freezetime && $ttFreeze += \Carbon\Carbon::parse($step->step_freezetime)->timestamp;
            $step->step_cooktime && $ttBake += \Carbon\Carbon::parse($step->step_cooktime)->timestamp;
            $step->step_fridgetime && $ttFridge += \Carbon\Carbon::parse($step->step_fridgetime)->timestamp;
        });

        $tt = $ttRealisation + $ttAtRest + $ttBake + $ttFreeze + $ttFridge;

        $recipe->stepsInfos = [
            'totalManipulation' =>  \Carbon\Carbon::parse($ttRealisation)->format('H:i'),
            'totalAtRest' => \Carbon\Carbon::parse($ttAtRest)->format('H:i'),
            'totalFreeze' => \Carbon\Carbon::parse($ttFreeze)->format('H:i'),
            'totalBake' => \Carbon\Carbon::parse($ttBake)->format('H:i'),
            'totalFridge' => \Carbon\Carbon::parse($ttFridge)->format('H:i'),
            'totalTime' => \Carbon\Carbon::parse($tt)->format('H:i'),
        ];

        return $recipe;
    }

    public static function getValidation()
    {
        return [
            'recipe_name' => 'required|string',
            'recipe_quantity' => 'required|numeric|min:1',
            'recipe_utensils.*' => 'string|min:1',
            'recipe_utensils_str' => 'string|max:1000',
            'recipe_keywords.*' => 'string|min:1',
            'recipe_keywords_str' => 'string|max:200',
            'recipe_isalcoholic' => 'required|boolean',
            'recipe_isvisible' => 'required|boolean',
            'recipe_platetype.platetype_id' => 'required|exists:App\Models\PlateType,platetype_id',
            'recipe_country.country_id' => 'exists:App\Models\Country,country_id',
            'recipe_regime.regime_id' => 'exists:App\Models\Regime,regime_id',
            'recipe_targets' => 'array|max:3',
            'recipe_targets.*.target_id' => 'exists:App\Models\Target,target_id',
            'recipe_steps' => 'array|required|min:1',
            'recipe_steps.*.step_name' => 'string|required|max:50',
            'recipe_steps.*.step_description' => 'string|required|min:100|max:5000',
            'recipe_steps.*.step_realisationtime' => [
                'string',
                'required',
                Rule::notIn(['00:00']),
                "regex:'^[0-9]{2,7}:[0-9]{2}$'",
            ],
            'recipe_steps.*.ingredients.*.composedby_ingredientname' => 'required|string|max:200',
            'recipe_steps.*.ingredients.*.quantity' => [
                'required',
                "regex:'^[0-9]*(((\.|\,)[0-9]+)|((\/[1-9][0-9]*)|(\/0+[1-9]+[0-9]*)))?$'",
            ]
        ];
    }

    public function store(Request $request)
    {
        try{
            $req = array_merge($request->all(), [
                'recipe_utensils_str' => implode(',', array_unique($request->recipe_utensils)),
                'recipe_keywords_str' => implode(',', array_unique($request->recipe_keywords))
            ]);

            $validator = Validator::make($req, RecipeController::getValidation());

            if($validator->fails()){
                return $this->sendError('Error validation', $validator->errors(), 500);       
            }

            $recipeDatas = [
                "recipe_name" => $request->recipe_name,
                "recipe_quantity" => $request->recipe_quantity,
                "recipe_utensils" => implode(',', array_unique($request->recipe_utensils)),
                "recipe_keywords" => implode(',', array_unique($request->recipe_keywords)),
                "recipe_isalcoholic" => !!$request->recipe_isalcoholic,
                "recipe_isvisible" => !!$request->recipe_isvisible,
                "recipe_platetypeid" => $request->recipe_platetype['platetype_id'],
            ];

            if($request->recipe_country){
                $recipeDatas["recipe_countryid"] = $request->recipe_country['country_id'];
            }
            if($request->recipe_regime){
                $recipeDatas["recipe_regimeid"] = $request->recipe_regime['regime_id'];
            }

            \DB::beginTransaction();
            $recipe = auth()->user()->recipes()->create($recipeDatas);

            if(!$recipe){
                return $this->sendError('Error creating recipe', 'Impossible de créer la recette', 500);       
            }

            if($request->recipe_image) {
                $image = $request->recipe_image;
                
                $startImageCode = 'data:image/png;base64,';
                if(substr($image, 0, strlen($startImageCode)) !== $startImageCode){
                    return $this->sendError('Picture validation', 'Le format de l\'image n\'est pas valide', 500);       
                }

                $image = str_replace($startImageCode, '', $image);
                $image = str_replace(' ', '+', $image);
                $fileName = 'recipes/' . $recipe->recipe_id . '.' . RecipeController::RECIPE_IMAGE_EXT;
                Storage::disk('local')->put('public/'.$fileName, base64_decode($image), 'public');
                $recipe->recipe_image = $fileName;
                $recipe->save();
            }

            $targets = [];
            foreach($request->recipe_targets ?: [] as $target){
                $targets[] = $target['target_id'];
            }
            $recipe->targets()->attach(array_unique($targets));

            foreach($request->recipe_steps as $i => $requestStep){
                $step = $recipe->steps()->create([
                    "step_position" => $i+1,
                    "step_name" => $requestStep['step_name'],
                    "step_description" => $requestStep['step_description'],
                    "step_realisationtime" => RecipeController::formatTime($requestStep['step_realisationtime']),
                    "step_sleeptime" => RecipeController::formatTime($requestStep['step_sleeptime']),
                    "step_fridgetime" => RecipeController::formatTime($requestStep['step_fridgetime']),
                    "step_freezetime" => RecipeController::formatTime($requestStep['step_freezetime']),
                    "step_cooktime" => RecipeController::formatTime($requestStep['step_cooktime']),
                ]);

                foreach($requestStep['ingredients'] as $composedBy){
                    $qte = explode('/', $composedBy['quantity']);
                    if(!isset($qte[1])){
                        $qte[]=1;
                    }
                    $qte = $qte[0] / max($qte[1], 1);

                    if(!is_numeric($qte)){
                        return $this->sendError('Quantity error', "La quantité : $qte n'est pas valide", 500);       
                    }

                    $step->composedBy()->create([
                        "composedby_ingredientname" => $composedBy['composedby_ingredientname'],
                        "quantity" => $qte,
                        "mesure" => $composedBy['mesure'],
                    ]);                
                }
            }
            
            \DB::commit();
            return $recipe;
        } catch (\Illuminate\Database\QueryException $e){
            \DB::rollback();
            $errorMsg = $e->errorInfo[2];
            return $this->sendError('Creation query exception', $e, 500);                
        } catch (\Exception $e){
            \DB::rollback();
            return $this->sendError('Creation server exception', $e->getMessage(), 500);
        }
    }

    public static function formatTime($time){
        if(empty($time) || $time === '00:00'){
            return null;
        }

        $time = explode(':', $time);

        if(!isset($time[0]) || !isset($time[1])){
            return null;
        }

        if($time[1] >= 60){
            $allMins = $time[0] * 60 + $time[1];
            $time[0] = floor($allMins / 60);
            $time[1] = $allMins % 60;
        }

        return $time[0].':'.$time[1].':00';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Recipe $recipe)
    {
        return $recipe;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recipe $recipe)
    {
        $recipe->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recipe $recipe)
    {
        $recipe->delete();
    }
}
