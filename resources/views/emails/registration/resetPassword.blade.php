@component('mail::message')
# Bonjour {{ $name }},

<p>Pour finaliser le changement de votre mot de passe, veuillez valider la procédure en cliquant sur le bouton ci-dessous.</p>
@component('mail::button', ['url' => $token])
Valider le changement
@endcomponent
<p>Ce lien est valide pendant 30 minutes. Passée cette date, vous devrez recréer un nouveau mot de passe depuis la page de connexion.</p>

<p>Si vous n'êtes pas à l'origine de cette demande, vous pouvez simplement ignorer ce mail, et continuer à vous connecter avec votre mot de passe habituel.</p>

Toquement votre,<br>
{{ config('app.name') }}
@endcomponent
