<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\BadgeController;
use App\Http\Controllers\Api\CountryController;
use App\Http\Controllers\Api\RecipeController;
use App\Http\Controllers\Api\TargetController;
use App\Http\Controllers\Api\PlateTypeController;
use App\Http\Controllers\Api\RegimeController;
use App\Http\Controllers\Api\AuthController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// AUTHENTIFICATION
Route::post('login', [AuthController::class, 'signin']);
Route::post('register', [AuthController::class, 'signup']);
Route::post('validate', [AuthController::class, 'validateProfile']);
Route::post('revalidate', [AuthController::class, 'resendValidationMail']);

Route::prefix('reset-password')->group(function(){
    Route::post('/', [AuthController::class, 'createResetPassword']);
    Route::put('/', [AuthController::class, 'validateResetPassword']); 
});

// Auth middleware
Route::middleware('auth:sanctum')->group( function () {
    Route::prefix('token')->group(function(){
        Route::get('check', [AuthController::class, 'check']);
        Route::get('revoke', [AuthController::class, 'revoke']);
    });

    Route::prefix('recipe')->group(function(){
        Route::post('/', [RecipeController::class, 'store']);
    });
});

// Global routes
Route::get('recipes', [RecipeController::class, 'index']);

Route::prefix('target')->group(function () {
    Route::get('/', [TargetController::class, 'index']);
    Route::get('list', [TargetController::class, 'list']);
});

Route::get('countries', [CountryController::class, 'index']);

Route::prefix('regimes')->group(function () {
    Route::get('/', [RegimeController::class, 'index']);
});

Route::prefix('plate/types')->group(function () {
    Route::get('/', [PlateTypeController::class, 'index']);
});