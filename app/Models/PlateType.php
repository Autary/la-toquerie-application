<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlateType extends Model
{
    use HasFactory;
    protected $primaryKey = 'platetype_id';
    protected $table='platetype';
    public $timestamps = false;
}
