<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    use HasFactory;
    protected $primaryKey = 'recipe_id';
    protected $table='recipe';
    protected $badge;
    public $timestamps = false;

    protected $fillable = [
        "recipe_name",
        // "recipe_image",
        "recipe_quantity",
        "recipe_utensils",
        "recipe_keywords",
        "recipe_isalcoholic",
        "recipe_isvisible",
        "recipe_platetypeid",
        "recipe_countryid",
        "recipe_regimeid",
    ];

    public function badge()
    {
        return $this->hasOne(__NAMESPACE__.'\Badge', 'badge_id','recipe_badgeid');
    }
    public function country()
    {
        return $this->hasOne(__NAMESPACE__.'\Country', 'country_id','recipe_countryid');
    }
    public function regime()
    {
        return $this->hasOne(__NAMESPACE__.'\Regime', 'regime_id','recipe_regimeid');
    }
    public function plateType()
    {
        return $this->hasOne(__NAMESPACE__.'\PlateType', 'platetype_id','recipe_platetypeid');
    }
    public function pictures()
    {
        return $this->hasMany(__NAMESPACE__.'\Picture', 'recipe_pictureid');
    }
    public function user()
    {
        //ATTENTION, ne pas retourner le mail/mdp !
        return $this->belongsTo(__NAMESPACE__.'\User', 'recipe_profileid');
    }
    public function targets()
    {
        return $this->belongsToMany(__NAMESPACE__.'\Target', 'recipe_target','recipe_id','target_id');
    }
    public function steps()
    {
        return $this->hasMany(__NAMESPACE__.'\Step', 'step_recipeid');
    }
    public function notations()
    {
        return $this->hasMany(__NAMESPACE__.'\Notation', 'notation_recipeid');
    }
    //report
    public function scopeGetAverageNotations($q)
    {
        return $q->withCount(['notations as averageNotations' => function($query) {
            $query->select(\DB::raw('coalesce(round(avg(notation_note),1),0)'));
        }]);
    }
    public function scopeOrderByAverageNotations($q)
    {
        return $q->orderBy('averageNotations', 'desc');
    }
    public function scopePublicShowed($q){
        return $q->valid()->visible();
    }
    public function scopeValid($q){
        return $q->where("recipe_status","ok");
    }
    public function scopeVisible($q){
        return $q->where('recipe_isvisible', 1);
    }
    
}
